from django.shortcuts import render

def page404(request):
    return render(request, 'server/404.html', status=404)

def page500(request):
    return render(request, 'server/500.html', status=500)

def page422(request):
    return render(request, 'server/500.html', status=422)