# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StickyNotes'
        db.create_table('stickynotes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('note', self.gf('django.db.models.fields.CharField')(default='', max_length=2000, null=True)),
            ('created', self.gf('django.db.models.fields.IntegerField')(default=0, max_length=11, null=True)),
        ))
        db.send_create_signal(u'stickynotes', ['StickyNotes'])


    def backwards(self, orm):
        # Deleting model 'StickyNotes'
        db.delete_table('stickynotes')


    models = {
        u'stickynotes.stickynotes': {
            'Meta': {'object_name': 'StickyNotes', 'db_table': "'stickynotes'"},
            'created': ('django.db.models.fields.IntegerField', [], {'default': '0', 'max_length': '11', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'note': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '2000', 'null': 'True'})
        }
    }

    complete_apps = ['stickynotes']