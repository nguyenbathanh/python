from django.db import models

# Create your models here.
class StickyNotes(models.Model):
    note = models.CharField(max_length=2000, null=True, default='')
    created = models.IntegerField(max_length=11, null=True, default=0)
    #ip = models.CharField(max_length=30, null=True, default='')

    class Meta:
        db_table = 'stickynotes'

