from django.shortcuts import render
from django.http import HttpResponse
from myapp.stickynotes.models import StickyNotes
from myapp.utils import *
import json

def index(request):
    note_list = StickyNotes.objects.all().order_by('created')
    return render(request, 'stickynotes/index.html', {"note_list": note_list})

def add(request):
    if request.method == 'POST':
        response = {}
        try:
            unix_time = get_unix_timestamp()
            b2 = StickyNotes(created=unix_time)
            b2.save()
        except Exception, m:
            log_exception(m);
            response['s'] = False
        else:
            response['new_note'] = b2.id
            response['s'] = True
        finally:
            return HttpResponse(json.dumps(response), content_type='application/json')

    return HttpResponse(status=404)


def update(request):
    if request.method == 'POST':
        response = {}
        try:
            t = StickyNotes.objects.get(pk=request.POST['id'])
            t.note = request.POST['content']
            t.save()
        except Exception, m:
            log_exception(m)
            response['s'] = False
        else:
            response['s'] = True
        finally:
            return HttpResponse(json.dumps(response), content_type='application/json')

    return HttpResponse(status=404)

def remove(request):
    if request.method == 'POST':
        response = {}
        try:
            t = StickyNotes.objects.get(pk=request.POST['id'])
            t.delete()
        except Exception, m:
            log_exception(m)
            response['s'] = False
        else:
            response['s'] = True
        finally:
            return HttpResponse(json.dumps(response), content_type='application/json')

    return HttpResponse(status=404)
