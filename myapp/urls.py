from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       #url(r'^admin/', include(admin.site.urls)),
                       url(r'^stickynotes/add$', 'myapp.stickynotes.views.add'),
                       url(r'^stickynotes/update$', 'myapp.stickynotes.views.update'),
                       url(r'^stickynotes/remove$', 'myapp.stickynotes.views.remove'),
                       url(r'^contact/list$', 'myapp.contact.views.list'),
                       url(r'^contact/new$', 'myapp.contact.views.new'),
                       url(r'^contact/edit/(?P<id>\d+)$','myapp.contact.views.edit'),
                       # url(r'^contact?$', 'myapp.contact.views.test'),
                       url(r'^contact/upload$', 'myapp.contact.views.upload'),
                       url(r'^$', 'myapp.stickynotes.views.index')
)

# custom error pages (works only if DEBUG is set to FALSE)
handler404 = 'myapp.server.views.page404'
handler500 = 'myapp.server.views.page500'
handler422 = 'myapp.server.views.page422'
