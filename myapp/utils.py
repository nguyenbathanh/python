import os, settings, logging, datetime, time, Image

def get_unix_timestamp():
    n = datetime.datetime.now()
    return time.mktime(n.timetuple())

def handle_uploaded_file(f, resize=False):

    now = datetime.datetime.now()
    # print
    # print "Current date and time using str method of datetime object:"
    # print str(now)
    #
    # print
    # print "Current date and time using instance attributes:"
    # print "Current year: %d" % now.year
    # print "Current month: %d" % now.month
    # print "Current day: %d" % now.day
    # print "Current hour: %d" % now.hour
    # print "Current minute: %d" % now.minute
    # print "Current second: %d" % now.second
    # print "Current microsecond: %d" % now.microsecond
    #
    # print
    # print "Current date and time using strftime:"
    # print now.strftime("%Y-%m-%d %H:%M")
    file_storage = settings.STORAGE_LOCATION + str(now.year) + '/' + str(now.month) + '/' + str(now.day) + '/' + str(f.name)
    directory = get_file_directory(file_storage)

    if not os.path.exists(directory):
        os.makedirs(directory, 0777)

    thumb_directory = directory + '/thumb/'
    if not os.path.exists(thumb_directory):
        os.mkdir(thumb_directory)

    with open(file_storage, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    if resize:
        img = Image.open(file_storage)
        img = img.resize((230,230), Image.ANTIALIAS)
        img.save(thumb_directory+f.name)

def log_exception(m):
    logger = logging.getLogger('exception')
    logger.critical(m)

def log_warn(m):
    logger = logging.getLogger('warn')
    logger.warning(m)

def log_info(m):
    logger = logging.getLogger('info')
    logger.info(m)

def get_file_directory(filename):
    return filename[0:filename.rfind('/')]