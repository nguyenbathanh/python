from django.db import models

class Contact(models.Model):
    subject = models.CharField(max_length=100)
    message = models.CharField(max_length=1000, null=True, default='')
    sender = models.CharField(max_length=1000, null=True, default='')
    cc_myself = models.BooleanField(default=False)
    created = models.IntegerField(max_length=11, null=True, default=0)
    image = models.CharField(max_length=1000, null=True, default='')
    #address = models.CharField(max_length=500, null=True, default='')

    #@property
    def pa(self):
        return self.sender

    class Meta:
        db_table = 'contact'
        app_label = 'contact'
