from django import forms
from myapp.contact.models import Contact

class ContactForm(forms.ModelForm):
    subject = forms.CharField(widget=forms.TextInput)
    message = forms.CharField(widget=forms.Textarea, required=False)
    sender = forms.EmailField(widget=forms.TextInput)
    cc_myself = forms.BooleanField(required=False)
    created = forms.IntegerField(required=False)
    image = forms.FileField(required=True)

    def clean_sender(self):
        data = self.cleaned_data['sender']
        #if self.is_sender_exists(data):
        #    raise forms.ValidationError("Sender is already exists")
        # Always return the cleaned data, whether you have changed it or
        # not.
        return data

    def is_sender_exists(self, sender):
        return Contact.objects.filter(sender=sender).exists()

    def hightlight_error_elements(self):
        for f in self.errors:
            self.fields[f].widget.attrs.update({'class': 'error'})

    class Meta:
        model = Contact