from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from myapp.contact.forms import ContactForm
from myapp.contact.models import Contact
from myapp.utils import *
from django.contrib import messages

def list(request):
    from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
    all_contacts = Contact.objects.all()
    paginator = Paginator(all_contacts,5)
    page = request.GET.get('page')
    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        contacts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        contacts = paginator.page(paginator.num_pages)

    if request.method == 'POST':
        ids = request.POST['ids'].split(',')
        Contact.objects.filter(id__in=ids).delete()
        messages.add_message(request, messages.SUCCESS, 'Records deleted!')
        return HttpResponseRedirect('/contact/list')

    return render(request, 'contact/list.html',{'contacts': contacts, 'paginator': paginator, 'total': all_contacts.count})

def new(request):
    if request.method == 'POST': # If the form has been submitted...
        form = ContactForm(request.POST, request.FILES) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            t = form.save(commit=False)
            t.created = get_unix_timestamp()
            t.save()

            handle_uploaded_file(request.FILES['image'])

            messages.add_message(request, messages.SUCCESS, 'New record added')

            return HttpResponseRedirect('/contact/list') # Redirect after POST
        else:
            form.hightlight_error_elements()
    else:
        form = ContactForm() # An unbound form

    return render(request, 'contact/new.html', {'form': form})

def edit(request, id):
    if id:
        contact = get_object_or_404(Contact, pk=id)

    if request.method == 'POST':
        form = ContactForm(request.POST, instance=contact)
        if form.is_valid():
            t = form.save(commit=False)
            t.created = get_unix_timestamp()
            t.save()

            messages.add_message(request, messages.SUCCESS, 'Record updated!')
            #return HttpResponseRedirect('/contact/edit/'+id)
            return HttpResponseRedirect('/contact/list')
        else:
            form.hightlight_error_elements()
    else:
        form = ContactForm(instance=contact)

    return render(request, 'contact/edit.html', {'form': form, 'id': id})