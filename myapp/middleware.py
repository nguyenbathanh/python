import logging
from time import time
class ExceptionMiddleware(object):

    def process_request(self, request):
        request.timer = time()
        return None

    def process_exception(self, request, exception):
        logger = logging.getLogger('exception')
        logger.critical('%s %s %s',
            request.method,
            request.get_full_path(),
            exception,
        )

class DisableCSRF(object):
    def process_request(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)
